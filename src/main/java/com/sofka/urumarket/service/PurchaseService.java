package com.sofka.urumarket.service;

import com.sofka.urumarket.domain.dto.ProductPurchasedDto;
import com.sofka.urumarket.domain.dto.PurchaseDto;
import com.sofka.urumarket.domain.model.Client;
import com.sofka.urumarket.domain.model.Product;
import com.sofka.urumarket.domain.model.ProductPurchased;
import com.sofka.urumarket.domain.model.Purchase;
import com.sofka.urumarket.repository.ClientRepository;
import com.sofka.urumarket.repository.ProductPurchasedRepository;
import com.sofka.urumarket.repository.ProductRepository;
import com.sofka.urumarket.repository.PurchaseRepository;
import com.sofka.urumarket.service.interfaces.IPurchase;
import com.sofka.urumarket.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PurchaseService implements IPurchase {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PurchaseRepository purchaseRepository;
    @Autowired
    private ProductPurchasedRepository productPurchasedRepository;

    @Override
    public PurchaseDto create(PurchaseDto purchase) {
        var client = clientRepository.findById(purchase.getClient().getDocumentId());
        //Chequeo si el cliente existe.
        if (client.isPresent()) {
            List<ProductPurchasedDto> products = purchase
                    .getProductsPurchased()
                    .stream()
                    .peek(productPurchasedDto -> {
                        var p = productRepository.findById(productPurchasedDto.getProductId());
                        p.ifPresent(product -> {
                            productPurchasedDto.setProduct(product);
                            //productPurchasedDto.setPurchase(AppUtils.dtoToPurchase(purchase));
                        });
                    })
                    .collect(Collectors.toList());

            //Chequear que todos los productos estén habilitados.
            boolean correctProducts = this.realizarChequeos(products);
            if (correctProducts) {
                return createPurchase(products, purchase, client.get());
            }
        }
        return null;
    }

    private PurchaseDto createPurchase(List<ProductPurchasedDto> productDtos, PurchaseDto purchaseDto, Client client) {
        productDtos.forEach(productPurchased -> {
                    Product p = productPurchased.getProduct();
                    if (p.getInInventory() - productPurchased.getQuantity() == 0) {
                        p.setEnabled(false);
                        p.setInInventory(0);
                    } else if (p.getInInventory() - productPurchased.getQuantity() > 0) {
                        p.setInInventory(p.getInInventory() - productPurchased.getQuantity());
                    } else {
                        return;
                    }
                    productRepository.save(p);
        });

        purchaseDto.setProductsPurchased(productDtos);
        purchaseDto.setDate(LocalDateTime.now());
        purchaseDto.setClient(client);

        Purchase purchaseCreated = purchaseRepository.save(AppUtils.dtoToPurchase(purchaseDto));

        List<ProductPurchased> productsBought = productDtos.stream().map(productPurchasedDto -> {
                    ProductPurchased p = AppUtils.dtoToProductPurchased(productPurchasedDto);
                    p.setProduct(productPurchasedDto.getProduct());
                    p.setPurchase(purchaseCreated);
                    return productPurchasedRepository.save(p);
                })
                .collect(Collectors.toList());

        purchaseCreated.setProductsPurchased(productPurchasedRepository.saveAll(productsBought));
        purchaseRepository.save(purchaseCreated);
        return AppUtils.purchaseToDto(purchaseCreated);

    }

    @Override
    public PurchaseDto update(PurchaseDto purchase, Long id) {
        return null;
    }

    @Override
    public PurchaseDto delete(Long id) {
        return purchaseRepository
                .findById(id)
                .map(purchase -> {
                    purchaseRepository.delete(purchase);
                    return AppUtils.purchaseToDto(purchase);
                })
                .orElse(null);
    }

    @Override
    public List<PurchaseDto> findAll() {
        return purchaseRepository
                .findAll()
                .stream()
                .map(AppUtils::purchaseToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PurchaseDto findById(Long id) {
        var product = purchaseRepository.findById(id);
        return product
                .map(AppUtils::purchaseToDto)
                .orElse(null);
    }

    //Funciones de apoyo
    private boolean realizarChequeos(List<ProductPurchasedDto> products) {
        for (ProductPurchasedDto product : products) {
            if (!productsEnabled(product))
                return false;
            if (!checkInventory(product))
                return false;
            if (!checkMinAndMax(product))
                return false;
        }
        return true;
    }

    public boolean productsEnabled(ProductPurchasedDto product) {
        return product.getProduct().isEnabled();
    }

    private boolean checkMinAndMax(ProductPurchasedDto product) {
        return product.getProduct().getMax() >= product.getQuantity() && product.getProduct().getMin() <= product.getQuantity();
    }

    private boolean checkInventory(ProductPurchasedDto product) {
        return product.getProduct().getInInventory() >= product.getQuantity();
    }

}
