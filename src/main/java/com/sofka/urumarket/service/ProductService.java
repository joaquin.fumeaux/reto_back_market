package com.sofka.urumarket.service;

import com.sofka.urumarket.domain.dto.ProductDto;
import com.sofka.urumarket.domain.model.Product;
import com.sofka.urumarket.repository.ProductRepository;
import com.sofka.urumarket.service.interfaces.IProduct;
import com.sofka.urumarket.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService implements IProduct {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public ProductDto create(ProductDto product) {
        Product p = productRepository.save(AppUtils.dtoToProduct(product));
        return AppUtils.productToDto(p);
    }

    @Override
    public ProductDto update(ProductDto product, Long id) {
        return productRepository
                .findById(id)
                .map(product1 -> {
                    product.setId(id);
                    Product updatedProduct = productRepository.save(AppUtils.dtoToProduct(product));
                    return AppUtils.productToDto(updatedProduct);
                })
                .orElse(null);
    }

    @Override
    public ProductDto delete(Long id) {
        return productRepository
                .findById(id)
                .map(product -> {
                    productRepository.delete(product);
                    return AppUtils.productToDto(product);
                })
                .orElse(null);
    }

    @Override
    public List<ProductDto> findAll() {
        return productRepository
                .findAll()
                .stream()
                .map(AppUtils::productToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long id) {
        var product = productRepository.findById(id);
        return product
                .map(AppUtils::productToDto)
                .orElse(null);
    }
}
