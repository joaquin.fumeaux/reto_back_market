package com.sofka.urumarket.service;

import com.sofka.urumarket.domain.dto.ClientDto;
import com.sofka.urumarket.domain.model.Client;
import com.sofka.urumarket.repository.ClientRepository;
import com.sofka.urumarket.service.interfaces.IClient;
import com.sofka.urumarket.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService implements IClient {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDto create(ClientDto client) {
        Client c = clientRepository.save(AppUtils.dtoToClient(client));
        return AppUtils.clientToDto(c);
    }

    @Override
    public ClientDto update(ClientDto client, String documentId) {
        return clientRepository
                .findById(documentId)
                .map(client1 -> {
                    client.setDocumentId(documentId);
                    Client updatedClient = clientRepository.save(AppUtils.dtoToClient(client));
                    return AppUtils.clientToDto(updatedClient);
                })
                .orElse(null);
    }

    @Override
    public ClientDto delete(String documentId) {
        return clientRepository
                .findById(documentId)
                .map(client -> {
                    clientRepository.delete(client);
                    return AppUtils.clientToDto(client);
                })
                .orElse(null);
    }

    @Override
    public List<ClientDto> findAll() {
        return clientRepository
                .findAll()
                .stream()
                .map(AppUtils::clientToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ClientDto findById(String documentId) {
        var client = clientRepository.findById(documentId);
        return client
                .map(AppUtils::clientToDto)
                .orElse(null);
    }
}
