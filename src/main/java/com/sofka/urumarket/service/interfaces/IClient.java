package com.sofka.urumarket.service.interfaces;

import com.sofka.urumarket.domain.dto.ClientDto;

import java.util.List;

public interface IClient {
    ClientDto create(ClientDto client);
    ClientDto update(ClientDto client, String documentId);
    ClientDto delete(String documentId);
    List<ClientDto> findAll();
    ClientDto findById(String documentId);
}
