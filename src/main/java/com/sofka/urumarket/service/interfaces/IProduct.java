package com.sofka.urumarket.service.interfaces;

import com.sofka.urumarket.domain.dto.ProductDto;

import java.util.List;

public interface IProduct {
    ProductDto create(ProductDto product);
    ProductDto update(ProductDto product, Long id);
    ProductDto delete(Long id);
    List<ProductDto> findAll();
    ProductDto findById(Long id);
}
