package com.sofka.urumarket.service.interfaces;

import com.sofka.urumarket.domain.dto.ProductPurchasedDto;

import java.util.List;

public interface IProductPurchased {
    ProductPurchasedDto create(ProductPurchasedDto productPurchasedDto);
    ProductPurchasedDto findById(Long id);
    List<ProductPurchasedDto> findAll();
    void delete(Long id);
    List<ProductPurchasedDto> findProductsPurchasedByClientId(String documentId);
}
