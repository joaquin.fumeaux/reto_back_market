package com.sofka.urumarket.service.interfaces;

import com.sofka.urumarket.domain.dto.PurchaseDto;
import java.util.List;

public interface IPurchase {
    PurchaseDto create(PurchaseDto purchase);
    PurchaseDto update(PurchaseDto purchase, Long id);
    PurchaseDto delete(Long id);
    List<PurchaseDto> findAll();
    PurchaseDto findById(Long id);
}
