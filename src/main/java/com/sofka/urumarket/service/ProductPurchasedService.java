package com.sofka.urumarket.service;

import com.sofka.urumarket.domain.dto.ProductPurchasedDto;
import com.sofka.urumarket.domain.model.ProductPurchased;
import com.sofka.urumarket.repository.ProductPurchasedRepository;
import com.sofka.urumarket.service.interfaces.IProductPurchased;
import com.sofka.urumarket.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductPurchasedService implements IProductPurchased {
    @Autowired
    private ProductPurchasedRepository productPurchasedRepository;


    @Override
    public ProductPurchasedDto create(ProductPurchasedDto productPurchasedDto) {
        ProductPurchased c = productPurchasedRepository.save(AppUtils.dtoToProductPurchased(productPurchasedDto));
        return AppUtils.productPurchasedToDto(c);
    }

    @Override
    public ProductPurchasedDto findById(Long id) {
        return productPurchasedRepository
                .findById(id)
                .map(AppUtils::productPurchasedToDto)
                .orElse(null);
    }

    @Override
    public List<ProductPurchasedDto> findAll() {
        return productPurchasedRepository
                .findAll()
                .stream()
                .map(AppUtils::productPurchasedToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        productPurchasedRepository.deleteById(id);
    }

    @Override
    public List<ProductPurchasedDto> findProductsPurchasedByClientId(String documentId){
        return productPurchasedRepository
                .findProductsPurchasedByClientId(documentId)
                .stream()
                .map(AppUtils::productPurchasedToDto)
                .collect(Collectors.toList());
    }

}
