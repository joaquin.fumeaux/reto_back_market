package com.sofka.urumarket.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClientDto {
    private String documentId;
    private String documentType;
    private String name;
    private String surname;
    private String phoneNumber;
    private String address;

    public ClientDto(String documentId) {
        this.documentId = documentId;
    }

    public ClientDto(String documentType, String name, String surname, String phoneNumber, String address) {
        this.documentType = documentType;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }
}
