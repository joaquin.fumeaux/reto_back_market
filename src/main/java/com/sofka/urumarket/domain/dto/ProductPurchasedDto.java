package com.sofka.urumarket.domain.dto;

import com.sofka.urumarket.domain.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductPurchasedDto {
    private Long id;
    private Long productId;
    private Integer quantity;
    private Product product;
}
