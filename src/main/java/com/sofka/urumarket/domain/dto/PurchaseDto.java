package com.sofka.urumarket.domain.dto;

import com.sofka.urumarket.domain.model.Client;
import com.sofka.urumarket.domain.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PurchaseDto {
    private Long id;
    private LocalDateTime date;
    private Client client;
    private List<Product> products;
    private List<ProductPurchasedDto> productsPurchased;
}
