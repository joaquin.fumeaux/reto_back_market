package com.sofka.urumarket.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private Integer inInventory;
    private boolean enabled;
    private Integer min;
    private Integer max;
}
