package com.sofka.urumarket.domain.model;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private LocalDateTime date;

    @OneToMany(
            fetch = FetchType.EAGER,
            targetEntity = ProductPurchased.class,
            mappedBy = "purchase")
    @JsonManagedReference
    private List<ProductPurchased> productsPurchased = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Client.class)
    private Client client;

}
