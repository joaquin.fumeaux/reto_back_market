package com.sofka.urumarket.domain.model;
import lombok.*;
import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Client {
    @Id
    @Column(name = "document_id", nullable = false)
    private String documentId;
    private String documentType;
    private String name;
    private String surname;
    private String phoneNumber;
    private String address;

    public Client(String documentId) {
        this.documentId = documentId;
    }
}
