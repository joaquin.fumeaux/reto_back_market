package com.sofka.urumarket.repository;

import com.sofka.urumarket.domain.model.ProductPurchased;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductPurchasedRepository extends JpaRepository<ProductPurchased, Long> {
    @Query(value = "SELECT purchase.client_document_id, product_purchased.* FROM purchase INNER JOIN product_purchased ON purchase.id = product_purchased.purchase_id \n" +
            "INNER JOIN client ON client.document_id = purchase.client_document_id\n" +
            "WHERE client.document_id = :id", nativeQuery = true)
    public List<ProductPurchased> findProductsPurchasedByClientId(@Param("id") String id);
}
