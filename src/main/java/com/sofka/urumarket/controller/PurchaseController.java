package com.sofka.urumarket.controller;

import com.sofka.urumarket.domain.dto.ProductPurchasedDto;
import com.sofka.urumarket.domain.dto.PurchaseDto;
import com.sofka.urumarket.service.ProductPurchasedService;
import com.sofka.urumarket.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private ProductPurchasedService productPurchasedService;

    @GetMapping("/findAll")
    public ResponseEntity<List<PurchaseDto>> findAll() {
        List<PurchaseDto> purchases = purchaseService.findAll();
        if (purchases.size() > 0) {
            return new ResponseEntity<>(purchases, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/create")
    public ResponseEntity<PurchaseDto> create(@RequestBody PurchaseDto purchaseDto){
        PurchaseDto purchaseCreated = purchaseService.create(purchaseDto);
        if(purchaseCreated != null){
            return new ResponseEntity<>(purchaseCreated, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new PurchaseDto(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/getPurchases")
    public ResponseEntity<List<ProductPurchasedDto>> getPurchases(){
        List<ProductPurchasedDto> productPurchasedDtoList = productPurchasedService.findAll();
        return new ResponseEntity<>(productPurchasedDtoList, HttpStatus.OK);
    }

    @GetMapping("/getPurchasesByClientId/{documentId}")
    public ResponseEntity<List<ProductPurchasedDto>> getPurchasesByClientId(@PathVariable("documentId") String documentId){
        List<ProductPurchasedDto> productPurchasedDtoList = productPurchasedService.findProductsPurchasedByClientId(documentId);
        System.out.println(productPurchasedDtoList);
        return new ResponseEntity<>(productPurchasedDtoList, HttpStatus.OK);
    }
}
