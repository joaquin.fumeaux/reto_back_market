package com.sofka.urumarket.controller;

import com.sofka.urumarket.domain.dto.ClientDto;
import com.sofka.urumarket.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping("/findAll")
    public ResponseEntity<List<ClientDto>> findAll() {
        List<ClientDto> clients = clientService.findAll();
        if (clients.size() > 0) {
            return new ResponseEntity<>(clients, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/findById/{documentId}")
    public ResponseEntity<ClientDto> findById(@PathVariable("documentId") String documentId) {
        ClientDto client =  clientService.findById(documentId);
        if(client != null){
            return new ResponseEntity<>(client, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/create")
    public ResponseEntity<ClientDto> create(@RequestBody ClientDto clientDto){
        return new ResponseEntity<>(clientService.create(clientDto), HttpStatus.CREATED);
    }

    @PutMapping("/update/{documentId}")
    public ResponseEntity<ClientDto> update(@RequestBody ClientDto clientDto, @PathVariable("documentId") String documentId){
        ClientDto client = clientService.update(clientDto, documentId);
        if(client != null){
            return new ResponseEntity<>(client, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

    @DeleteMapping("/delete/{documentId}")
    public ResponseEntity<ClientDto> delete(@PathVariable("documentId") String documentId){
        ClientDto client = clientService.delete(documentId);
        if(client != null){
            return new ResponseEntity<>(client, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
