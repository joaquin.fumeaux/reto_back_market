package com.sofka.urumarket.utils;

import com.sofka.urumarket.domain.dto.ClientDto;
import com.sofka.urumarket.domain.dto.ProductDto;
import com.sofka.urumarket.domain.dto.ProductPurchasedDto;
import com.sofka.urumarket.domain.dto.PurchaseDto;
import com.sofka.urumarket.domain.model.Client;
import com.sofka.urumarket.domain.model.Product;
import com.sofka.urumarket.domain.model.ProductPurchased;
import com.sofka.urumarket.domain.model.Purchase;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class AppUtils {
    //CLIENTS
    //CLIENT TO DTO
    public static ClientDto clientToDto(Client client){
        ClientDto clientDto = new ClientDto();
        BeanUtils.copyProperties(client, clientDto);
        return clientDto;
    }
    //DTO TO CLIENT
    public static Client dtoToClient(ClientDto clientDto){
        Client client = new Client();
        BeanUtils.copyProperties(clientDto, client);
        return client;
    }

    //PRODUCTS
    //PRODUCT TO DTO
    public static ProductDto productToDto(Product product){
        ProductDto productDto = new ProductDto();
        BeanUtils.copyProperties(product, productDto);
        return productDto;
    }
    //DTO TO PRODUCT
    public static Product dtoToProduct(ProductDto productDto){
        Product product = new Product();
        BeanUtils.copyProperties(productDto, product);
        return product;
    }

    //PURCHASES
    //PURCHASE TO DTO
    public static PurchaseDto purchaseToDto(Purchase purchase){
        PurchaseDto purchaseDto = new PurchaseDto();
        BeanUtils.copyProperties(purchase, purchaseDto);
        return purchaseDto;
    }
    //DTO TO PURCHASE
    public static Purchase dtoToPurchase(PurchaseDto purchaseDto){
        Purchase purchase = new Purchase();
        BeanUtils.copyProperties(purchaseDto, purchase);
        return purchase;
    }

    //PRODUCT PURCHASES
    //PRODUCTPURCHASE TO DTO
    public static ProductPurchasedDto productPurchasedToDto(ProductPurchased purchase){
        ProductPurchasedDto productPurchasedDto = new ProductPurchasedDto();
        BeanUtils.copyProperties(purchase, productPurchasedDto);
        return productPurchasedDto;
    }
    //DTO TO PRODUCTPURCHASE
    public static ProductPurchased dtoToProductPurchased(ProductPurchasedDto productPurchasedDto){
        ProductPurchased productPurchased = new ProductPurchased();
        BeanUtils.copyProperties(productPurchasedDto, productPurchased);
        return productPurchased;
    }

    public static List<ProductPurchased> DtoToList(List<ProductPurchasedDto> productPurchasedDtoList){
        return productPurchasedDtoList
                .stream()
                .map(AppUtils::dtoToProductPurchased)
                .collect(Collectors.toList());
    }
}
