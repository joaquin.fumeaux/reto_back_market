package com.sofka.urumarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrumarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrumarketApplication.class, args);
	}

}
