package com.sofka.urumarket.service;

import com.sofka.urumarket.domain.dto.ClientDto;
import com.sofka.urumarket.domain.model.Client;
import com.sofka.urumarket.repository.ClientRepository;
import com.sofka.urumarket.utils.AppUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class ClientServiceTest {

    @Autowired
    private ClientService clientService;

    @MockBean
    private ClientRepository clientRepository;

    @Test
    void create() {
        String documentId = "48485074";
        String documentType = "CU";
        String name = "Joaquin";
        String surname = "Fumeaux";
        String phoneNumber = "091490648";
        String address = "25 de Mayo 1186";

        Client client = new Client(documentId, documentType, name, surname, phoneNumber, address);
        ClientDto clientCreated = clientService.create(AppUtils.clientToDto(client));

        Assertions.assertEquals(clientCreated.getDocumentId(), client.getDocumentId());
        Assertions.assertEquals(clientCreated.getDocumentType(), client.getDocumentType());
        Assertions.assertEquals(clientCreated.getName(), client.getName());
        Assertions.assertEquals(clientCreated.getSurname(), client.getSurname());
        Assertions.assertEquals(clientCreated.getPhoneNumber(), client.getPhoneNumber());
        Assertions.assertEquals(clientCreated.getAddress(), client.getAddress());
    }

    @Test
    void update() {
        /*String documentId = "1111";
        String documentType = "CU";
        String name = "Lionel";
        String surname = "Messi";
        String phoneNumber = "12";
        String address = "Argentina";


        Optional<Client> clientrr = Optional.of(new Client(documentId, documentType, name, surname, phoneNumber, address));
        when(clientRepository.save(clientrr.get())).thenReturn(clientrr.get());
        when(clientRepository.findById("1111")).thenReturn(clientrr);
        ClientDto clientDto = new ClientDto( "CU", "Joaquin", "Fumeaux", "10", "1231sd");
        ClientDto clientUpdated = clientService.update(clientDto, "1111");

        Assertions.assertEquals("1121", clientUpdated.getDocumentId());*/



    }

    @Test
    void delete() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findById() {
    }
}