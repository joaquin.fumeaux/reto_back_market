# UruMarket
Este proyecto fue desarrollado utilizando una base de datos relacional con JPA, y Spring Boot.

## Pasos para poder correr el aplicativo.
1. En primer lugar se debe crear una base de datos llamada *urumarket* en Workbench o cualquier otro software similar.
2. Al correr la aplicación, tablas con sus respectivas relaciones se van a crear automáticamente.
3. También se puede importar el archivo `UruMarket.postman_collection.json` donde se podrán ver todas las peticiones a los endpoints.
4. Por otra parte, también se puede ver cada endpoint en Swagger, una vez quede corriendo el aplicativo  se puede ingresar a la siguiente ruta `http://localhost:8080/swagger-ui/index.html#/`

## Postman
![img.png](img.png)

Se creó la colección UruMarket y dentro de ella varias carpetas, donde se encuentran los endpoints correspondientes a cada controlador.